// working with sets and maps.
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <map>  // also known as an "associative array"
using std::map; // you can think of maps kind of like arrays,
				// but the indexes no longer have to be integers...
				// e.g., we could do A["a string"] = 100;


// read strings from standard input; print out a histogram of how
// frequently each one occurs.
void getHistogram() {
	/* NOTE: we need two datatypes to specify a map: one for the index
	 * set and one for the values. */
	map<string,int> M; // now M[s] is an integer for any string s.
	string s;
	while (cin >> s) {
		M[s]++;
		/* NOTE: by accessing M[s], we actually create a new entry,
		 * and furthermore, that entry will be initialized to 0.
		 * Unusual for so much to happen for you in C++, but this is
		 * how maps work.
		 * */
	}
	/* Now we need to figure out how to go through the contents of a map.
	 * Much like sets (and lots of other STL stuff) we will use iterators */
	map<string,int>::iterator i;
	/* NOTE: if you find yourself typing map<string,int>::blah too often,
	 * you can use a typedef statement to ease the pain:
	 * typedef map<string,int>::iterator mapit;
	 * Then we could use "mapit" to declare iterators. */
	for (i=M.begin(); i!=M.end(); i++) {
		// now what...  well, we want to print out *i.
		// but what is the datatype of *i ??  Whatever it is, it should
		// have both the string part, and the integer part.  and it does:
		// the datatype is pair<string,int>.  The pair class gives access
		// to the two members in variables called "first" and "second"
		cout << (*i).first << ":\t" << (*i).second << endl;
		// NOTE: we could also use the arrow syntax: i->first
		// Remember: (*i).something <===> i->something
	}
	// TODO: make this actually print a histogram instead of a
	// frequency table.
}


// TODO: finish the following "memoized" prime test:  Write a function that
// does the usual primality testing, but *remembers the answers* for values it
// has already computed an answer for.  For testing, add some cout statements
// to notify you when a value from the database is being used vs. when you are
// computing the result from scratch.

// We could use a map to store the database of primes:
map<unsigned long,bool> primes;

bool primetest(unsigned long x) {
	// before actually computing, see if we already know the answer:
	// one last map function: find(...)
	// evaluating primes[x] will actually *create* a value for x!
	// if (primes[x]) return true;  // this does not work...
	// we need a way to query the database without actually creating entries.
	map<unsigned long,bool>::iterator i;
	i = primes.find(x); // this tests if primes has an entry for x.
	if (i != primes.end()) { // then primes has an x entry
		// return the stored value.
	}
	// otherwise, we have to recompute the result, and SAVE THE ANSWER.
	if (x==3||x==2) {
		return true;
	}
	if (x<2 || x%2 == 0) {
		return false;
	}
	unsigned long j;
	for (j = 3; j < x; j+=2) {
		if (x%j==0)
			break;
	}
	return (j == x);
}

int main() {
	int whichTest = 0;
	if (whichTest == 0) {
		getHistogram();
	}
	else if (whichTest == 1) {
		unsigned long n;
		while (cin >> n) {
			cout << primetest(n) << endl;
		}
	}
	return 0;
}
