/* strings.cpp
 * More examples working with strings...
 * */
#include <iostream>
using std::cin;
using std::cout; 
using std::endl;
#include <string>
using std::string;

/* Example 1: write a function that takes a string and a character
 * and returns the number of occurences of the character in the string.
 * */

int countchr(string s,char c)
{
	unsigned long count=0;
	for(unsigned long i=0; i<s.length(); i++){
	
		if(c==s[i]){

			count++;
		}
		
	}
	return count;
}

/* NOTE: if you want to get a datatype of exactly a certain size,
 * you can use the header inttypes.h, which gives you int64_t, int32_t...
 * */


/* Example2: write a function that takes two strings and returns an
 * integer value indicating whether or not the first was a substring
 * of the second; the integer should be the index at which the string
 * was found, or -1 to indicate that the string was not found.
 * For example, findSubstr("def", "abcdef") would return 3.
 * */

int findSubstr(string s1, string s2)
{
	// TODO: there's a problem with the boundary
	// condition (the upper bound for the loop). Fix it.
	for (unsigned long i = 0; i < s2.length(); i++) {
		// in english, what is this supposed to do?
		// compare all of s1 with the substring of s2
		// starting at offset i.
		unsigned long j;
		for (j = 0; j < s1.length(); j++) {
			if (s1[j] != s2[i+j]) {
				break;
			}
		}
		// check: why did the loop end?
		if (j == s1.length()) // success 8D
			return i;
	}
	// if we ever end up here, what do we know?
	// search failed.
	return -1;
}

/* NOTE: there is a built-in string function for this (find(str,pos)).
 * TODO: try it out.
 * */

/* TODO: write a function that takes a string and returns a boolean
 * indicating whether or not it was a palindrome.
 * */

/* TODO: write a function that takes a string and returns another
 * string, which is the input reversed.
 * */

int main() {
	/* TODO: write test code for all of the functions... */
	return 0;
}
