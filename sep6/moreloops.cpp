/* September 6th, 2012
 * More about loops. */

// the usual stuff:
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;

// TODO: compile this via the makefile for a change, instead of directly with
// g++ on the command line.
int main()
{
	// New stuff: the for loop.
	/* syntax:
	   for(statementA; expressionB; statementC)
	   {
	   	   <arbitrary statements...>
	   }
	   1. statementA happens ONE time at the beginning.
	   2. expressionB is checked for true / false.
	   3. if expressionB evaluates to true, do the statements
	      in the body; otherwise exit the loop.
	   4. do statementC, and return to step 2.
	   // NOTE: no semicolon after statementC.
	*/
	// Example:  print a message 10 times.
	for (unsigned long i = 0; i < 10; i++) {
		cout << "annoying message...\n";
	}
	// cout << "check: i == " << i << "\n";
	// Note: the above won't actually compile, as the lifetime of
	// i is limited to the for loop.  Declare it outside, and try it.
	// TODO: translate the above loop into a while loop.
	// in general, it would look something like this:
	/*
		statement A
		while(expression B)
		{
			// do stuff
			statement C
		}
	*/

	// aside: remember that i++ is shorthand for i = i + 1.
	// NOTE: if i == 5, then i++ will evaluate to 5.
	// alternatively, the symbol ++i would evaluate to 6.


	// Example: compute the sum of the first 10 cubes:
	// 1+8+27+...+1000
	// NOTE: there's actually a simple closed-form solution,
	// but don't use that : )

	int total = 0;
	int ans;
	for(int num = 1; num<=10; num++)
	{
		ans = (num* num * num);
		total = total + ans ;
	}
	cout << total << endl;

	/* Example: compute the exponent (or the base 2 logarithm) of largest power
	 * of 2 that goes into an integer e.g., for 2 = 2^1, it would be 1.  For 4
	 * = 2^2, it would be 2, for 24 = 2^3*3, it would be 3, and for 20 = 2^2*5,
	 * we want 2.
	 */
	// how do we even check for divisibility??  easy way: use the modulus
	// operator, % warm up: check if an integer n is even:
	// if(n%2 == 0) { cout << "it is even.."; }
	
	// TODO: delete the following code and write it yourself from scratch.
	// (You can always use git to get the original back if you have trouble.)
	int n;
	cout << "Enter an integer to test: ";
	cin >> n;
	int p = 0;
	while (n%2 == 0) {
		p++;
		n = n/2;
	}
	cout << "largest power of 2 was 2^" << p << endl;
	// TODO: fix the case of n == 0, which causes an infinite loop!

	// for practice, let's do the same thing as a for loop
	cout << "Enter an integer to test: ";
	cin >> n;  // how many times is x divisible by 2?
	for (p = 0; n%2 == 0; p++) {
		n = n/2;
	}
	cout << "largest power of 2 was 2^" << p << endl;


	return 0;
}

// vim:foldlevel=4
