/* rectangle.h
 * This is the header file for a simple class for storing rectangles.
 * Here we define *what* a rectangle does, but we do *not* say anything
 * about *how* it is done.  That is left to the implementation file.
 * The header file should tell other programmers everything they need
 * to know in order to use our rectangle class.
 * */

#pragma once // this makes sure symbols here are not multiply defined.

#include <cstdlib> // needed for size_t

/* NOTE: our rectangles will only have integer sides. */
class rectangle
{
public:
	/* constructor: */
	rectangle(size_t w=1, size_t h=1);
	/* the above function is called *automatically* whenever
	 * someone creates a new rectangle instance.
	 * */

	size_t area(); // return the area
	size_t perimeter(); // return the perimeter
	bool isSquare(); // is this rectangle actually a square?
	void draw(); // print a crappy representation to the console.

	/* here are the member variables that we need to store
	 * the information that determines a rectangle:
	 * */
	size_t width;
	size_t height;

}; // annoying syntax: you need a semicolon here.  Don't forget.
