#include "Student.h"

StudentList::StudentList()
{
size = 0;
head = NULL;
}

StudentList::~StudentList()
{
Student * ptr = head;
Student * ptrToDelete;

while (ptr != NULL)
{
ptrToDelete = ptr;
ptr = ptr->next;
delete ptrToDelete;
}

}


void StudentList::display()
{
Student * ptr = head;

while ( ptr != NULL)
{
cout<<  ptr->id <<" "<< ptr->fname <<" "<< ptr->lname <<endl;

ptr  =  ptr->next;
}



}


void StudentList::add(Student s)
{
if(size==0)
{
head = new Student;
*head = s;

head->next = NULL;
}
else
{
Student * ptr = head;

//ptr->next really the same as (*ptr).next
while(ptr->next != NULL)
{
ptr = ptr->next;
}
ptr->next = new Student;
ptr = ptr->next;
*ptr = s;
ptr->next = NULL;
}
size++;

}
