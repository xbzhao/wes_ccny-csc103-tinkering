#include <iostream>
#include "Student.h"

using namespace std;

int main()
{

StudentList list;

Student stu;

stu.id = 89;
stu.fname = "Andy";
stu.lname = "Nagel";

list.add(stu);

stu.id = 99;
stu.fname = "Joe";
stu.lname = "Dimaggio";

list.add(stu);

stu.id = 8526;
stu.fname = "Tina";
stu.lname = "Fey";

list.add(stu);

list.display();

return 0;
}