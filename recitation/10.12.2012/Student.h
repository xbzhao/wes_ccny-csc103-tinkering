#include <iostream>
#include <new>
#include <cstdlib>

using  std::cout;
using std::string;
using std::endl;

class Student{

public:

int id;
string fname;
string lname;

Student * next;

};

class StudentList
{
private:
int size;
Student * head;

public:
StudentList();
~StudentList();

void add(Student s);
void display();


//Implement these if you can:
void display(int id); //should find the student in the list with id and display it
                       //or print "Not Found"
void remove(int id); //delete the student with this id, you will have to be careful
                     //to make sure the list stays connected.
void clear();  //deletes all students in list

Student get(int id);  //return the Student object with id

};
