#include "Complex.h"

Complex::Complex()
{
real = 0;
imagine = 0;
}

Complex::Complex(double r, double i)
{
real = r;
imagine = i;
}

void Complex::print()
{
cout<<real<<"+"<<imagine<<"i";
}

void Complex::add(Complex c)
{
real += c.real;
imagine += c.imagine;
}