#include <iostream>
using std::cout;

/*This is a class for representing and manipulating complex
numbers.  Implementation of the member functions are
in Complex.cpp.  But only the constructors, print() and add()
are implemented so far.

TODO:  Implement the rest of the functions, and test them
in a main program.
*/

class Complex
{
private:
double real;
double imagine;

public:
Complex();
Complex(double, double);
void print();
double getReal();
double getImagine();
void setReal(double);
void setImagine(double);
void add(Complex);
void sub(Complex);
void mult(Complex);
void divide(Complex);
};
