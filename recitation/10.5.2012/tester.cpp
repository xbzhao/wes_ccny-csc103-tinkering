#include "Complex.h"

int main()
{
Complex c;
c.print();
cout<<"\n";

Complex c2(3, 5);
Complex c3(4.5, 9.8);

c3.add(c2);
c3.print();

return 0;

}